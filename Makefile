# Configurações para facilitar a construção e uso do projeto

build:
	@docker image rmi onebytecode
	@docker-compose build

up:
	@docker-compose up -d

bash:
	@docker container exec -it ruby sh

down:
	@docker-compose down