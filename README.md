# Curso de Ruby Puro

Curso realizado referente a Trilha de Conhecimento da Neon.

## Pré-Condições

* Git
* Conta na AWS (AWS Translate)
* Docker

Para a Conta na AWS é preciso ter o `ACCESS_KEY_ID` e o `SECRET_ACCESS_KEY` já em mãos.

## Configurações

Antes de qualquer coisa, precisamos configurar um arquivo `.env` na raiz do projeto. Então na raiz `/` do projeto digite o comando :point_down:

> touch .env

Dentro desse arquivo inclua as seguintes variaveis :point_down:

```shell
AWS_ACCESS_KEY_ID=your_access_key_id
AWS_SECRET_ACCESS_KEY=your_secret_access_key
AWS_REGION=us-east-2
```

**Importante**: esses passos iniciais são importante para poder executar o _Projeto Final_!

### Subindo o projeto

> make build

Esse comando `make build` é responsavel por remover as images pré-existente com o nome de `onebitcode` e "buildar" uma nova imagem.

> make up

O comando `make up` sobe o _Container_ para uso do projeto

> make bash

Com o `make bash` você acessa o terminal _#!/bin/sh_ do projeto e a partir dessa ação pode usar para rodar os exercicios. Para saber como rodar os programas veja a proxima sessão.

> make down

Remove a instância criada do _Container_

### Rodandos os programas

Após o uso do `make bash`, use os comandos para rodar os programas

* Aula 2

> ruby aula_2/exec_1.rb  
> ruby aula_2/exec_2.rb  

* Aula 3

> ruby aula_3/exec_1.rb  

* Aula 4

> ruby aula_4/exec_1.rb  
> ruby aula_4/exec_2.rb
> ruby aula_4/exec_3.rb  

* Aula 5

> ruby aula_5/exec_1.rb  
> ruby aula_5/exec_2.rb  

* Aula 6

> ruby aula_6/exec_1.rb  

* Aula 7

> ruby aula_7/app.rb  

* Aula 8

> ruby aula_8/exec_1.rb  
> ruby aula_8/exec_2.rb  

* Aula 9

> ruby aula_9/exec_1.rb  
> ruby aula_9/exec_2.rb  

* Projeto Final

> ruby projeto_final/app.rb  

### Arquivos gerados do projeto final

Dentro do diretório `projeto_final` existe uma pasta `temp/`, onde a cada execução das traduções de texto irá ser criado arquivos `.txt` com as informações dessas traduções feita na AWS.

Arquivo consiste nas seguintes informações :point_down:

* Original text: <texto_original>  
* Translated text: <texto_traduzido>  
* Source language code: <tag_linguagem_original>  
* Target language code: <tag_linguagem_traduzida>  

As linguagens diponiveis são (Inglês, Português e Espanhol) pois não foi o foco ter diversos tipos de tradução nesse projeto e sim a estrutura realizada para a finalidade. Não sendo complexo a adição de novas opções de traduções.

## Referências

[Curso OneBitCode](https://onebitcode.com/course/ruby-puro/)  
[AWS Translate](https://docs.aws.amazon.com/pt_br/translate/latest/dg/what-is.html)  