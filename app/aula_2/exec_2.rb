# Crie um programa que receba dois números inteiros e no final exiba a soma, 
# a subtração, a adição e a divisão entre eles.

print 'Informe o primeiro número :> '
first_number = gets.chomp.to_i

print '\n'

print 'E o segundo número :> '
second_number = gets.chomp.to_i

print '\n'

#### Soma
sum = first_number + second_number
puts "A soma entre os números #{first_number} e #{second_number} é igual a #{sum}"

#### Subtração
subs = first_number - second_number
puts "A subtração entre os números #{first_number} e #{second_number} é igual a #{subs}"

#### Multiplicação
mult = first_number * second_number
puts "A multiplicação entre os números #{first_number} e #{second_number} é igual a #{mult}"

#### Divisão
if second_number.zero?
  puts 'Numero não pode ser dividido por 0 (zero)'
else
  div = first_number / second_number
  puts "A subtração entre os números #{first_number} e #{second_number} é igual a #{div}"
end