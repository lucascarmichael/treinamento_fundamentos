# Utilizando as estruturas de iteração e condição, crie uma calculadora que ofereça ao usuário a opção de Multiplicar, Dividir, Adicionar ou Subtrair dois números. Não se esqueça de também permitir que o usuário feche o programa.

while true
  print 'Informe o primeiro número :> '
  first_number = gets.chomp.to_i

  print 'E o segundo número :> '
  second_number = gets.chomp.to_i

  # MENU
  puts 'Opções:'
  puts '1 - Somar'
  puts '2 - Subtrair'
  puts '3 - Multiplicar'
  puts '4 - Dividir'
  puts '0 - Sair'

  print ':> '
  option = gets.chomp.to_i

  break if option.zero?

  case option
  when 1
    sum = first_number + second_number
    puts "A soma dos números é igual a #{sum}"
  when 2
    subs = first_number - second_number
    puts "A subtração dos números é igual a #{subs}"
  when 3
    mult = first_number * second_number
    puts "A multiplicação dos números é igual a #{mult}"
  when 4
    if second_number.zero?
      puts "Divisão Invalida! Divisor é igual a 0 (zero)"
    else
      div = first_number / second_number
      puts "A divisão dos números é igual a #{div}"
    end
  else
    puts 'Opção Invalida!'
  end
end