# Utilizando uma collection do tipo Array, escreva um programa que receba 3 números e no final exiba o resultado de cada um deles elevado a segunda potência.

numbers = Array.new
limit_values = 3

limit_values.times do |index|
  print "Entre com o #{index+1}° valor :> "
  numbers.push(gets.chomp.to_i)
end

potentiation = numbers.map do |number|
  number ** 2
end

puts "\nNúmeros na segunda potência => #{potentiation.join(', ')}"