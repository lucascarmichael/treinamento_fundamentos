# Dado o seguinte hash:
#
# Numbers = {a: 10, b: 30, c: 20, d: 25, e: 15}
#
# Crie uma instrução que seleciona o maior valor deste hash e no final imprima a chave e valor do elemento resultante.

numbers = { a: 10, b: 30, c: 20, d: 25, e: 15 }

# Quem eh o maior?
more_number = numbers.values.max

puts "Maior valor dentro do hash => #{more_number}"

# Quem eh o hash
whoami = numbers.select do |key, value|
  more_number.eql?(value)
end

puts "Chave: #{whoami.keys.join('')} Valor: #{whoami.values.join('')}"