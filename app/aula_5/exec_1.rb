# Crie um programa que possua um método que resolva a potência dado um número base e seu expoente. Estes dois valores devem ser informados pelo usuário.


def calcular_potencia_de_um_numero(numero, exp)
  numero.pow(exp)
end

# OUTPUT
print 'Informe o número :> '
numero = gets.chomp.to_i

print 'Informe o expoente :> '
exp = gets.chomp.to_i

resultado = calcular_potencia_de_um_numero(numero, exp)
puts "A potência de #{numero}^#{exp} é igual a #{resultado}"