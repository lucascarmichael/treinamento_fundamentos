# Siga a documentação da gem cpf_cnpj para criar um programa que recebe como entrada um número de cpf e em um  método verifique se este número é válido.
# 
# Link da documentação:
# 
# https://github.com/fnando/cpf_cnpj

require 'cpf_cnpj'

def valid_document?(number_cpf)
  CPF.valid?(number_cpf)
end

def formatted(number_cpf)
  cpf = CPF.new(number_cpf)
  cpf.formatted
end

# OUTPUT

print 'Informe seu CPF :> '
number_cpf = gets.chomp

if valid_document? number_cpf
  puts "O seu CPF #{formatted(number_cpf)} é Valido!"
else
  puts 'O documento informado não é valido'
end