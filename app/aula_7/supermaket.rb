class Supermaket

  def initialize(product)
    @product = product
  end

  def to_buy
    puts "Você comprou o produto #{@product.name} no valor de R$ #{@product.price}"
  end
end