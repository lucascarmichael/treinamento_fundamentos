require 'cpf_cnpj'
require 'faker'

module Person

  class Person
    attr_accessor :name
  
    def initialize(name)
      @name = name
    end
    
    def valid_document?; raise 'Abstract Method' end
    def formatted; raise 'Abstract Method' end
  
  
    def message_document(obj)
      puts "Documento Invalido da #{obj.name}"
    end
  end

  class Physical < Person
    attr_accessor :cpf
    
    def initialize(name, cpf)
      super(name)
      @cpf = cpf
    end

    def valid_document?
      CPF.valid?(@cpf)
    end

    def formatted
      cpf = CPF.new(@cpf)
      cpf.formatted
    end

    def add
      return message_document(self) unless valid_document?

      puts 'Pessoa Física Adicionada'
      puts "Nome: #{@name}"
      puts "CPF: #{formatted}"
    end
  end
  
  class Juridic < Person
    attr_accessor :cnpj    

    def initialize(name, cnpj)
      super(name)
      @cnpj = cnpj
    end

    def valid_document?
      CNPJ.valid?(@cnpj)
    end

    def formatted
      cnpj = CNPJ.new(@cnpj)
      cnpj.formatted
    end

    def add
      return message_document(self) unless valid_document?

      puts 'Pessoa Jurídica Adicionada'
      puts "Nome: #{@name}"
      puts "CNPJ: #{formatted}"
    end
  end
end

Person::Physical.new(Faker::Name.name, CPF.generate).add
puts ''
Person::Juridic.new(Faker::Name.name, CNPJ.generate).add