require_relative 'translate'
require_relative 'helpers/file_helper'

def translate
  puts 'Selecione a Opção:'
  puts '1 - Português'
  puts '2 - Inglês'
  puts '3 - Espanhol'

  option = gets.chomp.to_i

  case option
  when 1 then 'pt'
  when 2 then 'en'
  when 3 then 'es'
  end
end

def menu
  puts 'Informe qual o idioma do texto?'
  past_lang = translate
  puts 'Informe qual o idioma a ser traduzido?'
  to_lang = translate
  print 'Informe o texto :> '
  text = gets.chomp.to_s

  Translate.new({ text: text, past_lang: past_lang, to_lang: to_lang })
end

### APP
client = menu
response = client.translate

### OUTPUT
include FILE
Write.new.response_translate(client.text, response)

puts ''
puts "Texto original: #{client.text}"
puts "Texto Traduzido: #{response.translated_text}"