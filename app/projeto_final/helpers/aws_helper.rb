module AWS
  def credentials
    {
      access_key_id: ENV['AWS_ACCESS_KEY_ID'],
      secret_access_key: ENV['AWS_SECRET_ACCESS_KEY'],
      region: ENV['AWS_REGION']
    } 
  end

  def request_text(text, past_lang, to_lang)
    {
      text: text,
      source_language_code: past_lang,
      target_language_code: to_lang    
    }
  end
end