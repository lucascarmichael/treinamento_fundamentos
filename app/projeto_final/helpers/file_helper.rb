module FILE

  class Write
    def response_translate(text, response)
      file_name = named_file_with_the_date_time_of_now
      File.open("projeto_final/tmp/#{file_name}.txt", "w") do |file|
        file.write("Original text: #{text} \n")
        file.write("Translated text: #{response[:translated_text]} \n")
        file.write("Source language code: #{response[:source_language_code]} \n")
        file.write("Target language code: #{response[:target_language_code]} \n")
      end
    end

    def named_file_with_the_date_time_of_now
      time = Time.now
      time.strftime('%d-%m-%y_%H:%M:%S')
    end
  end
  
end