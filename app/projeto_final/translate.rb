require 'aws-sdk-translate'
require_relative 'helpers/aws_helper'


class Translate

  include AWS
  
  attr_reader :text

  def initialize(params)
    @text = params[:text]
    @past_lang = params[:past_lang]
    @to_lang = params[:to_lang]
  end

  def translate
    client = instance_aws
    client.translate_text(request)
  end

  private
  def instance_aws
    Aws::Translate::Client.new(credentials)
  end
  
  def request
    request_text(@text, @past_lang, @to_lang)
  end
end
