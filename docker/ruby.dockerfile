FROM ruby:3.0.2-alpine3.14

RUN apk add --no-cache --update \
            build-base

RUN gem update --system
RUN gem install bundle

WORKDIR /usr/src/app

COPY ./app .

RUN bundle install